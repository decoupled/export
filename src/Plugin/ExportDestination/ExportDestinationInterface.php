<?php

namespace Drupal\export\Plugin\ExportDestination;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a common interface for all ExportDestination objects.
 */
interface ExportDestinationInterface {

    public function getDestination();

}
