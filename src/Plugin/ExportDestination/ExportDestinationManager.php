<?php

namespace Drupal\export\Plugin\ExportDestination;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages export format plugins.
 *
 * @see \Drupal\export\Annotation\ExportDestination
 */
class ExportDestinationManager extends DefaultPluginManager {

  /**
   * Constructs a new ExportDestinationManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ExportDestination',
      $namespaces,
      $module_handler,
      'Drupal\export\Plugin\ExportDestination\ExportDestinationInterface',
      'Drupal\export\Annotation\ExportDestination'
    );
  }

}
