<?php

namespace Drupal\export\Plugin\ExportDestination;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\export\Plugin\ExportDestination\ExportDestinationInterface;

/**
 * Defines a common interface for all ExportDestination objects.
 */
abstract class ExportDestinationBase extends PluginBase implements ExportDestinationInterface {

    public function getFormat() {}

    public function getSource() {}

}
