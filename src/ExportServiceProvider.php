<?php

namespace Drupal\export;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\export\Batch\ExportBatch;
use Drupal\export\Plugin\ExportDestination\ExportDestinationManager;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers services in the container.
 */
class ExportServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    $container->register('export.destination_manager', ExportDestinationManager::class)
      //      ->setArguments(['@container.namespaces', '@module_handler']);
      ->addArgument(new Reference('container.namespaces'))
      ->addArgument(new Reference('module_handler'));

//    $container->register('export_entity.exports_subscriber', ExportEntityGetExportsSubscriber::class)
//      ->addTag('event_subscriber');
//
//    $container->register('export_entity.destination_subscriber', ExportEntityGetDestinationSubscriber::class)
//      ->addTag('event_subscriber');

  }

}
