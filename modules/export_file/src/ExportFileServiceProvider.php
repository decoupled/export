<?php

namespace Drupal\export_file;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\export_file\Service\ExportFileStorage;

/**
 * Registers services in the container.
 */
class ExportFileServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    $container->register('export_file.storage', ExportFileStorage::class);
  }

}
