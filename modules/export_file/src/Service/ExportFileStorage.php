<?php

namespace Drupal\export_file\Service;

use Drupal\Core\File\FileSystemInterface;

/**
 * Class ExportFileStorage
 *
 * @TODO: Move writing data to an event subscriber.
 *
 * @package Drupal\export_file\Service
 */
class ExportFileStorage {

  public function exists($path) {
    return !is_dir($path) && file_exists($path);
  }

  public function get($path) {
    return file_get_contents($path);
  }

  public function set($data, $path) {
    $directory = static::determineDirectory($path);
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    \Drupal::service('file_system')->saveData($data, $path, FileSystemInterface::EXISTS_REPLACE);
  }

  public function copy($source, $path) {
    $directory = static::determineDirectory($path);
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    \Drupal::service('file_system')->copy($source, $path, FileSystemInterface::EXISTS_ERROR);
  }

  public function remove($path) {
    \Drupal::service('file_system')->delete($path);

    $directory = static::determineDirectory($path);
    $files_in_directory = file_scan_directory($directory, '/.*/');

    if (count($files_in_directory) === 0) {
      \Drupal::service('file_system')->deleteRecursive($directory);
    }

    $debug = 1;
  }

  protected function determineDirectory($path) {
    $destination = explode('/', $path);
    array_pop($destination);
    return implode('/', $destination);
  }

}
