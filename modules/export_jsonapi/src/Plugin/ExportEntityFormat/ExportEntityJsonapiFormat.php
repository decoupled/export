<?php

namespace Drupal\export_jsonapi\Plugin\ExportEntityFormat;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatInterface;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatBase;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatInterface;

/**
 * @ExportEntityFormat(
 *   id = "jsonapi",
 *   label = @Translation("JSON:API"),
 *   description = @Translation("JSON:API Format."),
 * )
 */
class ExportEntityJsonapiFormat extends ExportEntityFormatBase implements ExportEntityFormatInterface {

  public function getFormat() {
    return 'json';
  }

  public function getSource(EntityInterface $entity) {
    /** @var \Drupal\jsonapi\ResourceType\ResourceType $resource_type */
    $resource_type = \Drupal::service('jsonapi.resource_type.repository')->get($entity->getEntityTypeId(), $entity->bundle());
    $source = '/' . \Drupal::service('url_generator')->getPathFromRoute('jsonapi.' . $resource_type->getTypeName() . '.individual', ['entity' => $entity->uuid()]);
    return $source;
  }

}
