<?php

namespace Drupal\export_json\Plugin\ExportEntityFormat;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatInterface;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatBase;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatInterface;

/**
 * @ExportEntityFormat(
 *   id = "json",
 *   label = @Translation("JSON"),
 *   description = @Translation("JSON Format."),
 * )
 */
class ExportEntityJsonFormat extends ExportEntityFormatBase implements ExportEntityFormatInterface {

  public function getFormat() {
    return 'json';
  }

  public function getSource(EntityInterface $entity) {
    return $entity->toUrl()->toString() . '?_format=json';
  }

}
