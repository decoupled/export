<?php

namespace Drupal\export_html\EventSubscriber;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\export_entity\Event\ExportEntityDetermineDependentPathsEvent;
use Drupal\export_entity\Event\ExportEntityTransformDataEvent;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\PathPlaceholderEvent;
use Drupal\tome_static\Event\TomeStaticEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes HTML export data.
 */
class ExportEntityDetermineDependentPathsSubscriber implements EventSubscriberInterface {

  /**
   * Reacts to a collect paths event.
   *
   * @param \Drupal\export_entity\Event\ExportEntityTransformDataEvent $event
   *   The collect paths event.
   */
  public function determineDependentPaths(ExportEntityDetermineDependentPathsEvent $event) {
    if ($event->getExport()->get('target')->entity->get('format') === 'html') {
      $data = $event->getData();

      $paths = $this->getHtmlAssets($data);
      $event->setPaths($paths);
    }
  }

  /**
   * Finds and exports assets for the given HTML content.
   *
   * @param string $content
   *   An HTML string.
   * @param string $root
   *   A root path to resolve relative paths.
   *
   * @return array
   *   An array of paths found in the given HTML string.
   */
  protected function getHtmlAssets($content, $root = 'http://backend.next.localhost:8888/') {
    $paths = [];
    $document = new \DOMDocument();
    @$document->loadHTML($content);
    $xpath = new \DOMXPath($document);
    /** @var \DOMElement $image */
    foreach ($xpath->query('//img | //source') as $image) {
      if ($image->hasAttribute('src')) {
        $paths[] = $image->getAttribute('src');
      }
      if ($image->hasAttribute('srcset')) {
        $srcset = $image->getAttribute('srcset');
        $sources = explode(' ', preg_replace('/ [^ ]+(,|$)/', '', $srcset));
        foreach ($sources as $src) {
          $paths[] = $src;
        }
      }
    }
    $rels = [
      'stylesheet',
      'shortcut icon',
      'icon',
      'image_src',
    ];
    /** @var \DOMElement $node */
    $links = $document->getElementsByTagName('link');
    foreach ($document->getElementsByTagName('link') as $node) {
      if (in_array($node->getAttribute('rel'), $rels, TRUE) && $node->hasAttribute('href')) {
        $paths[] = $node->getAttribute('href');
      }
    }
    /** @var \DOMElement $node */
    $anchors = $document->getElementsByTagName('a');
    foreach ($document->getElementsByTagName('a') as $node) {
      if ($node->hasAttribute('href')) {
        $paths[] = $node->getAttribute('href');
      }
    }
    /** @var \DOMElement $node */
    $scripts = $document->getElementsByTagName('script');
    foreach ($document->getElementsByTagName('script') as $node) {
      if ($node->hasAttribute('src')) {
        $paths[] = $node->getAttribute('src');
      }
    }
    /** @var \DOMElement $node */
    $styles = $document->getElementsByTagName('style');
    foreach ($document->getElementsByTagName('style') as $node) {
      $foo = $this->getCssAssets($node->textContent, $root);


      $paths = array_merge($paths, $this->getCssAssets($node->textContent, $root));
    }

    $other_styles = $xpath->query('//*[@style]');
    foreach ($xpath->query('//*[@style]') as $node) {
      $bar = $this->getCssAssets($node->getAttribute('style'), $root);

      $paths = array_merge($paths, $this->getCssAssets($node->getAttribute('style'), $root));
    }

    foreach (array_filter($paths, function($path) {
      return strpos(strtok($path, '?'), '.css') === strlen(strtok($path, '?')) - 4;
    }) as $css_path) {

      try {
        // Fetch other path data.
        $response = \Drupal::service('http_client')->get($this->getHost() . $css_path);
        $css_content = $response->getBody()->getContents();
        $paths = array_merge($paths, $this->getCssAssets($css_content, $this->getHost()));

        $foo_bar_baz = 1;
      } catch (\Exception $e) {
        // Do something useful here.
        throw $e;
      }

    }
    return $this->filterInvokePaths($paths);


  }

  /**
   * Finds assets for the given CSS content.
   *
   * @param string $content
   *   A CSS string.
   * @param string $root
   *   A root path to resolve relative paths.
   *
   * @return array
   *   An array of paths found in the given CSS string.
   */
  protected function getCssAssets($content, $root) {
    $paths = [];
    // Regex copied from the Static module from Drupal 7.
    // Credit to Randall Knutson and Michael Vanetta.
    $matches = [];
    preg_match_all('/url\(\s*[\'"]?(?!(?:data)+:)([^\'")]+)[\'"]?\s*\)/i', $content, $matches);
    if (isset($matches[1])) {
      $paths = $matches[1];
    }
    $paths = $this->getRealPaths($paths, $root);
    return $paths;
  }

  /**
   * Turns relative paths into absolute paths.
   *
   * Useful specifically for CSS's url().
   *
   * @param array $paths
   *   An array of paths to convert.
   * @param string $root
   *   A root path to resolve relative paths.
   *
   * @return array
   *   An array of converted paths.
   */
  protected function getRealPaths(array $paths, $root) {
    $root_dir = dirname($this->sanitizePath($root));
    foreach ($paths as &$path) {
      if (strpos($path, '../') !== FALSE) {
        $path = $this->joinPaths($root_dir, $path);
      }
    }
    return $paths;
  }

  /**
   * Sanitizes a given path by removing hashes, get params, and extra slashes.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   The sanitized path.
   */
  protected function sanitizePath($path) {
    $path = preg_replace(['/\?.*/', '/#.*/'], '', $path);
    return ltrim($path, '/');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['export_entity.determine_dependent_paths'][] = ['determineDependentPaths'];
    return $events;
  }

  /**
   * Joins multiple paths.
   *
   * Original credit to Riccardo Galli.
   *
   * @see https://stackoverflow.com/a/15575293
   *
   * @return string
   *   The joined path.
   */
  protected function joinPaths() {
    $paths = [];

    foreach (func_get_args() as $arg) {
      if ($arg !== '') {
        $paths[] = $arg;
      }
    }

    return rtrim(preg_replace('#(?<!:)/+#', '/', implode('/', $paths)), '/');
  }

  /**
   * Filters invoke paths to remove any external or cached paths.
   *
   * @param array $invoke_paths
   *   An array of paths returned by requestPath or exportPaths.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   *
   * @return array
   *   An array of paths to invoke.
   */
  protected function filterInvokePaths(array $invoke_paths) {
    $invoke_paths = preg_replace(['/^#.*/', '/#.*/'], ['/', ''], $invoke_paths);

    $path_alias_manager = \Drupal::service('path.alias_manager');

    foreach ($invoke_paths as $i => &$invoke_path) {
      $invoke_path = $this->makeExternalUrlLocal($invoke_path);
      if (UrlHelper::isExternal($invoke_path)) {
        unset($invoke_paths[$i]);
        continue;
      }
      $components = parse_url($invoke_path);
      // @TODO: Remove path alias paths.
      if ($path_alias_manager->getPathByAlias($components['path']) !== $components['path']) {
        unset($invoke_paths[$i]);
        continue;
      }
      if (substr($components['path'], -1) === '/') {
        unset($invoke_paths[$i]);
        continue;
      }
      if (isset($components['query'])) {
        parse_str($components['query'], $query);
        if (isset($query['destination'])) {
          unset($query['destination']);
        }
        $query = http_build_query($query);
        $invoke_path = $components['path'];
        if (!empty($query)) {
          $invoke_path .= '?' . $query;
        }
      }
    }

    return array_values(array_unique($invoke_paths));
  }

  /**
   * Makes external URLs local if their hostname is the current hostname.
   *
   * @param string $path
   *   A path.
   *
   * @return string
   *   A possibly transformed path.
   */
  protected function makeExternalUrlLocal($path) {
    $components = parse_url($path);
    if (UrlHelper::isExternal($path) && isset($components['host']) && UrlHelper::externalIsLocal($path, \Drupal::service('request_stack')->getCurrentRequest()->getSchemeAndHttpHost())) {
      $path = $components['path'];
      if (!empty($components['query'])) {
        $path .= '?' . $components['query'];
      }
    }
    return $path;
  }

  /**
   * @return string
   */
  protected function getHost() {
    $request = \Drupal::request();
    $host = $request->getHost();
    $scheme = $request->getScheme();
    $port = $request->getPort() ?: 80;
    if (('http' == $scheme && $port == 80) || ('https' == $scheme && $port == 443)) {
      $http_host = $scheme . '://' . $host;
    } else {
      $http_host = $scheme . '://' . $host . ':' . $port;
    }
    return $http_host;
  }

}
