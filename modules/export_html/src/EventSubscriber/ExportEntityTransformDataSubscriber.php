<?php

namespace Drupal\export_html\EventSubscriber;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\export_entity\Event\ExportEntityTransformDataEvent;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\PathPlaceholderEvent;
use Drupal\tome_static\Event\TomeStaticEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Processes HTML export data.
 */
class ExportEntityTransformDataSubscriber implements EventSubscriberInterface {

  /**
   * Reacts to a collect paths event.
   *
   * @param \Drupal\export_entity\Event\ExportEntityTransformDataEvent $event
   *   The collect paths event.
   */
  public function transformData(ExportEntityTransformDataEvent $event) {
    if ($event->getExport()->get('target')->entity->get('format') === 'html') {
      $data = $event->getData();

      $paths = [];
      $document = new \DOMDocument();
      @$document->loadHTML($data);
      $xpath = new \DOMXPath($document);

      /** @var \DOMElement $node */
      $anchors = $document->getElementsByTagName('a');
      foreach ($document->getElementsByTagName('a') as $node) {
        if ($node->hasAttribute('href')) {
          $paths[] = $node->getAttribute('href');
        }
      }

      $paths = $this->filterInvokePaths($paths);
      foreach($paths as $path) {
        // Rewrite included links to prevent a redirect on navigation.
        $data = str_replace($path . '"', $path . '/"', $data);
      }

      $event->setData($data);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['export_entity.transform_data'][] = ['transformData'];
    return $events;
  }

  /**
   * Filters invoke paths to remove any external or cached paths.
   *
   * @param array $invoke_paths
   *   An array of paths returned by requestPath or exportPaths.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   *
   * @return array
   *   An array of paths to invoke.
   */
  protected function filterInvokePaths(array $invoke_paths) {
    $invoke_paths = preg_replace(['/^#.*/', '/#.*/'], ['/', ''], $invoke_paths);

    $path_alias_manager = \Drupal::service('path.alias_manager');

    foreach ($invoke_paths as $i => &$invoke_path) {
      $invoke_path = $this->makeExternalUrlLocal($invoke_path);
      if (UrlHelper::isExternal($invoke_path)) {
        unset($invoke_paths[$i]);
        continue;
      }
      $components = parse_url($invoke_path);
      // @TODO: Remove path alias paths.
      if ($path_alias_manager->getPathByAlias($components['path']) === $components['path']) {
        unset($invoke_paths[$i]);
        continue;
      }
      if (substr($components['path'], -1) === '/') {
        unset($invoke_paths[$i]);
        continue;
      }
    }

    return array_values(array_unique($invoke_paths));
  }

  /**
   * Makes external URLs local if their hostname is the current hostname.
   *
   * @param string $path
   *   A path.
   *
   * @return string
   *   A possibly transformed path.
   */
  protected function makeExternalUrlLocal($path) {
    $components = parse_url($path);
    if (UrlHelper::isExternal($path) && isset($components['host']) && UrlHelper::externalIsLocal($path, \Drupal::service('request_stack')->getCurrentRequest()->getSchemeAndHttpHost())) {
      $path = $components['path'];
      if (!empty($components['query'])) {
        $path .= '?' . $components['query'];
      }
    }
    return $path;
  }

}
