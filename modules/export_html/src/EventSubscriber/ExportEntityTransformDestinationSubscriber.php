<?php

namespace Drupal\export_html\EventSubscriber;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\export_entity\Event\ExportEntityTransformDataEvent;
use Drupal\export_entity\Event\ExportEntityTransformDestinationEvent;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\PathPlaceholderEvent;
use Drupal\tome_static\Event\TomeStaticEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Processes HTML export data.
 */
class ExportEntityTransformDestinationSubscriber implements EventSubscriberInterface {

  /**
   * Reacts to a collect paths event.
   *
   * @param \Drupal\export_entity\Event\ExportEntityTransformDestinationEvent $event
   *   The collect paths event.
   */
  public function transformDestination(ExportEntityTransformDestinationEvent $event) {

    $destination = $event->getDestination();

    if (strpos($destination, '.html') === strlen($destination) - 5) {

      $destination = substr($destination, 0, -5) . '/index.html';
      $destination = str_replace('//index.html', '/index.html', $destination);

      $event->setDestination($destination);
      $debug = 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['export_entity.transform_destination'][] = ['transformDestination'];
    return $events;
  }

}
