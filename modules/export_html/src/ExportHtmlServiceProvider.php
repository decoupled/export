<?php

namespace Drupal\export_html;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\export_entity\Event\ExportEntityDetermineDependentPathsEvent;
use Drupal\export_html\EventSubscriber\ExportEntityDetermineDependentPathsSubscriber;
use Drupal\export_html\EventSubscriber\ExportEntityProcessDataSubscriber;
use Drupal\export_html\EventSubscriber\ExportEntityTransformDataSubscriber;
use Drupal\export_html\EventSubscriber\ExportEntityTransformDestinationSubscriber;
use Drupal\export_request\EventSubscriber\PageCacheRequestPrepareSubscriber;
use Drupal\export_request\PageCache\RequestPolicy\DynamicRequestPolicy;
use Drupal\export_request\Service\ExportRequestProcessExportHandler;
use Drupal\export_request\StackMiddleware\ResettablePageCache;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers services in the container.
 */
class ExportHtmlServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    $container->register('export_html.transform_data_subscriber', ExportEntityTransformDataSubscriber::class)
      ->addTag('event_subscriber');

    $container->register('export_html.transform_destination_subscriber', ExportEntityTransformDestinationSubscriber::class)
      ->addTag('event_subscriber');

    $container->register('export_html.determine_dependent_paths_subscriber', ExportEntityDetermineDependentPathsSubscriber::class)
      ->addTag('event_subscriber');
  }

}
