<?php

namespace Drupal\export_html\Plugin\ExportEntityFormat;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatBase;
use Drupal\export\Plugin\ExportFormat\ExportFormatInterface;
use Drupal\export_entity\Entity\ExportEntityTarget;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatBase;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatInterface;

/**
 * @ExportEntityFormat(
 *   id = "html",
 *   label = @Translation("HTML"),
 *   description = @Translation("HTML Format."),
 * )
 */
class ExportEntityHtmlFormat extends ExportEntityFormatBase implements ExportEntityFormatInterface {

  public function getFormat() {
    return 'html';
  }

  public function getSource(EntityInterface $entity) {
    return $entity->toUrl()->toString();
  }

  public function getDestination(EntityInterface $entity, ExportEntityTarget $export_entity_target, $destination) {
    $namespace = $export_entity_target->get('namespace') ?: $export_entity_target->id();
    $alias = (bool) $export_entity_target->get('alias');
    return $destination . $namespace . ($alias ? $entity->toUrl()->toString() : $this->getSource($entity)) . '/index.' . $this->getFormat();
  }

}
