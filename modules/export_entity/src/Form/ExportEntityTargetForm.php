<?php

namespace Drupal\export_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

class ExportEntityTargetForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $target */
    $target = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $target->label(),
      '#description' => $this->t("The name of this export target."),
      '#required' => TRUE,
      '#weight' => 10,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $target->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$target->isNew(),
      '#weight' => 11,
    ];

    $types = array_map(function($entity_type) {
      return $entity_type->getLabel();
    }, \Drupal::entityTypeManager()->getDefinitions());

    $form['types'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity types'),
      '#description' => $this->t('Select entity types to export.'),
      '#default_value' => $target->get('types'),
      '#options' => $types,
      '#multiple' => TRUE,
      '#weight' => 15
    ];

    $formats = [];
    foreach (\Drupal::service('export_entity.format_manager')->getDefinitions() as $format_plugin) {
      $formats[$format_plugin['id']] = $format_plugin['label'];
    }

    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#description' => $this->t('Select the export format for this export target.'),
      '#default_value' => $target->get('format'),
      '#options' => $formats,
      '#weight' => 30,
    ];

    $destinations = [];
    foreach (\Drupal::service('export.destination_manager')->getDefinitions() as $id => $destination_plugin) {
      $destinations[$id] = $destination_plugin['admin_label'];
    }

    $form['destination'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination'),
      '#description' => $this->t('Configure the export destination.'),
      '#default_value' => $target->get('destination'),
      '#options' => $destinations,
      '#weight' => 40,
    ];

    $form['namespace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Namespace'),
      '#description' => $this->t('No leading & no ending slash please.'),
      '#default_value' => $target->get('namespace'),
      '#weight' => 50,
    ];

    $form['alias'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Alias'),
      '#description' => $this->t('Use entity path aliases (if available).'),
      '#default_value' => $target->get('alias'),
      '#weight' => 50,
    ];

    $form['instant'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Instant export update'),
      '#description' => $this->t('Update exports immediately upon source changes like entity updates.'),
      '#default_value' => $target->get('instant'),
      '#weight' => 55,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Status'),
      '#description' => $this->t('Enable or disable this export target.'),
      '#default_value' => $target->get('status'),
      '#weight' => 60,
    ];

    if ($target->id()) {
      $form['update_warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [t('Configuration changes on this form will clear exports & necessitate a subsequent <a href="@bulk_url">bulk export</a> operation.', [
            '@bulk_url' => Url::fromRoute('export_entity.bulk_export', [
              'export_entity_target' => $target->id(),
            ])->toString(),
          ])],
        ],
        '#status_headings' => ['warning' => t('Warning message')],
        '#weight' => -201,
      ];
    }

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $target = $this->entity;
    $status = $target->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label target.', [
        '%label' => $target->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label target was not saved.', [
        '%label' => $target->label(),
      ]));
    }

    $form_state->setRedirect('entity.export_entity_target.collection');
  }

  /**
   * Helper function to check whether an entity target configuration entity exists.
   */
  public function exist($id) {
    $entity = \Drupal::service('entity_type.manager')->getStorage('export_entity_target')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
