<?php

namespace Drupal\export_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\export_entity\Entity\ExportEntityTarget;
use Drupal\export_entity\Service\ExportEntityHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * A form to bulk purge exports.
 */
class ExportEntityTargetBulkPurgeForm extends ConfirmFormBase {

  /**
   * The export entity target
   *
   * @var \Drupal\export_entity\Entity\ExportEntityTarget
   */
  protected $export_entity_target;

  /**
   * The export entity handler
   *
   * @var \Drupal\export_entity\Service\ExportEntityHandler
   */
  protected $exportEntityHandler;

  /**
   * The entity type manager service
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\export_entity\Form\ExportEntityTargetBulkExportForm $form_object */
    $form_object = parent::create($container);
    $form_object->setExportEntityHandler($container->get('export_entity.handler'));
    $form_object->setEntityTypeManager($container->get('entity_type.manager'));
    $form_object->setMessenger($container->get('messenger'));

    return $form_object;
  }

  /**
   * Set the export entity handler.
   *
   * @param \Drupal\export_entity\Service\ExportEntityHandler $export_entity_handler
   *   The export entity handler.
   */
  public function setExportEntityHandler(ExportEntityHandler $export_entity_handler) {
    $this->exportEntityHandler = $export_entity_handler;
  }

  /**
   * Set the entity type manager service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_entity.bulk_purge';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ExportEntityTarget $export_entity_target = NULL) {
    $this->export_entity_target = $export_entity_target;
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $export_entity_target = $this->export_entity_target;

    if ($export_entity_target->get('types')) {
      $entities = [];
      foreach ($export_entity_target->get('types') as $entity_type) {
        $entities_by_type = $this->entityTypeManager->getStorage($entity_type)->loadMultiple();
        asort($entities_by_type);
        $entities = array_merge($entities, $entities_by_type);
      }
      $this->exportEntityHandler->deleteMultiple($entities, $export_entity_target, FALSE);
      $this->messenger->addStatus(
        $this->t('Bulk purge successful.')
      );
      $form_state->setRedirectUrl($export_entity_target->toUrl( 'edit-form'));
    } else {
      $this->messenger->addStatus(
        $this->t('No entity types configured for this export entity target.')
      );
      $form_state->setRedirectUrl($export_entity_target->toUrl( 'edit-form'));
    }
  }

  /**
   * Returns the question to ask the user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return t('Are you sure you want to bulk purge exports for this export entity target?');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return $this->export_entity_target->toUrl( 'edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Bulk purge');
  }
}
