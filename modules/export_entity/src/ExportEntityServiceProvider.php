<?php

namespace Drupal\export_entity;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\export_entity\Batch\ExportEntityBatchBulkUtility;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatManager;
use Drupal\export_entity\Service\ExportEntityHandler;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers services in the container.
 */
class ExportEntityServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    $container->register('export_entity.handler', ExportEntityHandler::class);

    $container->register('export_entity.format_manager', ExportEntityFormatManager::class)
      //      ->setArguments(['@container.namespaces', '@module_handler']);
      ->addArgument(new Reference('container.namespaces'))
      ->addArgument(new Reference('module_handler'));

//    $container->register('export_entity.batch', ExportEntityBatchBulkUtility::class);
//      ->addArgument(new Reference('entity_type.manager'));

//    $container->register('export_entity.exports_subscriber', ExportEntityGetExportsSubscriber::class)
//      ->addTag('event_subscriber');
//
//    $container->register('export_entity.destination_subscriber', ExportEntityGetDestinationSubscriber::class)
//      ->addTag('event_subscriber');

  }

}
