<?php

namespace Drupal\export_entity\Batch;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\export_entity\Entity\ExportEntityExport;
use Drupal\export_entity\Entity\ExportEntityTarget;
use Drupal\export_entity\Event\ExportEntityDetermineDependentPathsEvent;
use Drupal\export_entity\Event\ExportEntityExtractPathsEvent;
use Drupal\export_entity\Event\ExportEntityProcessDataEvent;
use Drupal\export_entity\Event\ExportEntityTransformDataEvent;
use Drupal\export_entity\Event\ExportEntityTransformDestinationEvent;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ExportEntityBatchUtility {

  public static function setup(&$context = []) {
    if (empty($context['results']) && empty($context['results']['start'])) {
      $context['results']['start'] = (new \DateTime)->getTimestamp();
    }

    // Defer messages.
    static::deferMessages($context);
  }

  /**
   * @param $success
   * @param $results
   */
  public static function finish($success, $results, $operations) {
    $timestring = static::getTimeString($results['start']);

    if ($success) {
      \Drupal::service('messenger')->addMessage(new TranslatableMarkup('A total of @total exports were processed in @timestring.', [
        '@total' => count($results['exports']),
        '@timestring' => $timestring,
      ]), MessengerInterface::TYPE_STATUS);
    }
  }

  public static function addEntities($entities, &$context = []) {
    // Setup batch operation context.
    if (empty($context['results']) || empty($context['results']['entities'])) {
      $context['results']['entities'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    // Add entities.
    $context['results']['entities'] = array_merge(
      $context['results']['entities'],
      $entities
    );
  }

  public static function loadEntities($entity_type, $entity_ids, $amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($entity_ids);
    }
    if (empty($context['results']) || empty($context['results']['entities'])) {
      $context['results']['entities'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    // Load entities.
    $context['results']['entities'] = array_merge(
      $context['results']['entities'],
      \Drupal::service('entity_type.manager')->getStorage($entity_type)->loadMultiple(
        array_slice($entity_ids, $context['sandbox']['progress'], $amount_per_execution, TRUE)
      )
    );

    // Track the progress index.
    $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

    // Display progress message.
    $context['message'] = '<br />' . new TranslatableMarkup('Loaded @progress of @total entities.', [
      '@progress' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['max'],
    ]);

    // Check if this operation is finished.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function determineExports($export_entity_target, $instant_update, $amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['entities']);
    }
    if (empty($context['results']) || !isset($context['results']['exports']) || !isset($context['results']['exclude'])) {
      $context['results']['exports'] = [];
      $context['results']['exclude'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    // Determine exports.
    $entities = $context['results']['entities'];
    foreach (array_slice($entities, $context['sandbox']['progress'], $amount_per_execution, TRUE) as $entity) {
      if ($export_entity_target) {
        $exports = static::determine($entity, $export_entity_target);
        $context['results']['exports'] = array_merge(
          $context['results']['exports'],
          $exports
        );
        foreach($exports as $export) {
          $context['results']['exclude'][$export->id()] = $export->id();
        }
      } else {
        $export_entity_targets = static::determineTargets($entity, $instant_update);
        foreach ($export_entity_targets as $export_entity_target) {
          $exports = static::determine($entity, $export_entity_target);
          $context['results']['exports'] = array_merge(
            $context['results']['exports'],
            $exports
          );
          foreach($exports as $export) {
            $context['results']['exclude'][$export->id()] = $export->id();
          }
        }
      }
    }

    // Track the progress index.
    $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

    // Display progress message.
    $context['message'] = '<br />' . new TranslatableMarkup('Determined exports for @progress of @total entities.', [
      '@progress' => $context['sandbox']['progress'] < $context['sandbox']['max'] ? $context['sandbox']['progress'] : $context['sandbox']['max'],
      '@total' => $context['sandbox']['max'],
    ]);

    // Check if this operation is finished.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function loadExports($export_entity_export_ids, $amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($export_entity_export_ids);
    }
    if (empty($context['results']) || !isset($context['results']['exports']) || !isset($context['results']['exclude'])) {
      $context['results']['exports'] = [];
      $context['results']['exclude'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['sandbox']['max']) {
      $context['finished'] = 1;
    } else {
      // Load exports.
      $exports = \Drupal::service('entity_type.manager')
        ->getStorage('export_entity_export')
        ->loadMultiple(
          array_slice($export_entity_export_ids, $context['sandbox']['progress'], $amount_per_execution, TRUE)
        );
      $context['results']['exports'] = array_merge(
        $context['results']['exports'],
        $exports
      );
      foreach ($exports as $export) {
        $context['results']['exclude'][$export->id()] = $export->id();
      }

      // Track the progress index.
      $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

      // Display progress message.
      $context['message'] = '<br />' . new TranslatableMarkup('Loaded @progress of @total exports.', [
          '@progress' => $context['sandbox']['progress'] < $context['sandbox']['max'] ? $context['sandbox']['progress'] : $context['sandbox']['max'],
          '@total' => $context['sandbox']['max'],
        ]);

      // Check if this operation is finished.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  public static function determineDependentExports($export_entity_target, $instant_update, $amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['results']) || !isset($context['results']['dependents_queried']) || !isset($context['results']['dependent_ids'])) {
      $context['results']['dependents_queried'] = FALSE;
      $context['results']['dependent_ids'] = [];
    }
    if (empty($context['results']) || !isset($context['results']['dependents'])) {
      $context['results']['dependents'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['results']['dependents_queried']) {
      // Build query to determine dependent exports.
      $query = \Drupal::entityQuery('export_entity_export');

      // Exclude handled exports.
      if (isset($context['results']['exclude']) && count($context['results']['exclude']) > 0) {
        $exclude_exports_conditions = $query->andConditionGroup();
        foreach ($context['results']['exclude'] as $exclude_export_id) {
          $exclude_exports_conditions->condition('id', $exclude_export_id, '<>');
        }
        $query->condition($exclude_exports_conditions);
      }

      /** @var EntityInterface $entity */
      $cache_tags_conditions = $query->orConditionGroup();
      $export_entity_target_conditions = $query->orConditionGroup();
      if ($export_entity_target && $export_entity_target->get('status')) {
        $export_entity_target_conditions->condition('target', $export_entity_target->id());
      }
      foreach ($context['results']['entities'] as $entity) {
        $cache_tags_to_invalidate = $entity->getCacheTagsToInvalidate();
        if (count($cache_tags_to_invalidate) > 0) {
          // Query for cache tag dependence.
          foreach ($cache_tags_to_invalidate as $cache_tag_to_invalidate) {
            $cache_tags_conditions->condition('tags',"%<${cache_tag_to_invalidate}>%", 'LIKE');
          }
        }
        if (!$export_entity_target) {
          $export_entity_targets = static::determineTargets($entity, $instant_update);
          if (!empty($export_entity_targets)) {
            foreach ($export_entity_targets as $export_entity_target) {
              if ($export_entity_target->get('status')) {
                $export_entity_target_conditions->condition('target', $export_entity_target->id());
              }
            }
          }
        }
      }
      if ($cache_tags_conditions->count()) {
        $query->condition($cache_tags_conditions);
      }
      if ($export_entity_target_conditions->count()) {
        $query->condition($export_entity_target_conditions);
      }

      try {
        $context['results']['dependent_ids'] = $query->execute();
      } catch (\Exception $e) {
        // TODO: do something useful here.
        throw $e;
      }

      $context['results']['dependents_queried'] = TRUE;
    }
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['dependent_ids']);
    }

    // Recompute dependent export entity exports as configuration might have
    // been updated since the last export.
    if (count($context['results']['dependent_ids']) > 0) {
      /** @var \Drupal\Core\Entity\EntityStorageInterface $export_storage */
      $export_entity_export_storage = \Drupal::service('entity_type.manager')->getStorage('export_entity_export');

      $dependent_ids = $context['results']['dependent_ids'];
      foreach ($export_entity_export_storage->loadMultiple(array_slice($dependent_ids, $context['sandbox']['progress'], $amount_per_execution, TRUE)) as $dependent_export) {
        $dependent_export_entity_target = $dependent_export->get('target')->entity;
        list($dependent_entity_type, $dependent_entity_id) = explode(':', $dependent_export->get('entity')->value);
        $dependent_entity = \Drupal::service('entity_type.manager')->getStorage($dependent_entity_type)->load($dependent_entity_id);
        $context['results']['dependents'] = array_merge(
          $context['results']['dependents'],
          static::determine($dependent_entity, $dependent_export_entity_target)
        );
      }
    }

    // Track the progress index.
    $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

    // Display progress message.
    $context['message'] = '<br />' . new TranslatableMarkup('Determined dependent exports for @progress of @total entities.', [
        '@progress' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['max'],
      ]);

    // Check if this operation is finished.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function processExports($amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['exports']);
    }

    // Defer messages.
    static::deferMessages($context);

    // Process exports.
    $exports = array_slice($context['results']['exports'], $context['sandbox']['progress'], $amount_per_execution, TRUE);
    array_walk($exports, function($export) {
      static::export($export);


    });

    // Track the progress index.
    $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

    // Display progress message.
    $context['message'] = '<br />' . new TranslatableMarkup('Processed @progress of @total exports.', [
      '@progress' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['max'],
    ]);

    // Check if this process is finished.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function processDependentExports($amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['dependents']);
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['sandbox']['max']) {
      $context['finished'] = 1;
    } else {
      // Process exports.
      $dependents = array_slice($context['results']['dependents'], $context['sandbox']['progress'], $amount_per_execution, TRUE);
      array_walk($dependents, function($export) {
        static::export($export);
      });

      // Track the progress index.
      $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

      // Display progress message.
      $context['message'] = '<br />' . new TranslatableMarkup('Processed @progress of @total dependent exports.', [
          '@progress' => $context['sandbox']['progress'],
          '@total' => $context['sandbox']['max'],
        ]);

      // Check if this process is finished.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  public static function removeExports($amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['exports']);
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['sandbox']['max']) {
      $context['finished'] = 1;
    } else {
      // Process exports.
      $exports = array_slice($context['results']['exports'], $context['sandbox']['progress'], $amount_per_execution, TRUE);
      array_walk($exports, function ($export) {
        static::remove($export);
      });

      // Track the progress index.
      $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

      // Display progress message.
      $context['message'] = '<br />' . new TranslatableMarkup('Processed @progress of @total exports.', [
          '@progress' => $context['sandbox']['progress'],
          '@total' => $context['sandbox']['max'],
        ]);

      // Check if this process is finished.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * @param $export
   */
  protected static function export(ExportEntityExport $export) {
    // Fetch export data.
    list(
      'data' => $data,
      'tags' => $tags,
      'expiry' => $expiry
      ) = \Drupal::service('export_request.handler')->handleExport($export);

    // Clean up previous export if export destination has changed.
    if ($export->id()) {
      /** @var \Drupal\export_entity\Entity\ExportEntityExport $original_export */
      $original_export = \Drupal::service('entity_type.manager')
        ->getStorage('export_entity_export')
        ->loadUnchanged($export->id());
      if ($original_export->get('destination')->value !== $export->get('destination')->value) {
        \Drupal::service('export_file.storage')
          ->remove($original_export->get('destination')->value);
      }
    }

    $event = new ExportEntityDetermineDependentPathsEvent($data, $export);
    \Drupal::service('event_dispatcher')->dispatch('export_entity.determine_dependent_paths', $event, $export);
    $paths = $event->getPaths();

    // @TODO: Make it possible to hook into here, e.g. to transform the data
    // before saving, rewrite static asset paths and/or rewrite pager links.
    $event = new ExportEntityTransformDataEvent($data, $export);
    \Drupal::service('event_dispatcher')->dispatch('export_entity.transform_data', $event, $export);
    $data = $event->getData();

    // Set export data.
    \Drupal::service('export_file.storage')
      ->set($data, $export->get('destination')->value);

    foreach ($paths as $path) {

      // Remove possible query parameters.
      $path = strtok($path, '?');

      // @TODO: check if path not in export/source table column & not already in
      // target destination thingy. If neither, request path & either write it
      // or move it to target destination.
      $query = \Drupal::entityQuery('export_entity_export');
      if ($export->id()) {
        $query->condition('id', $export->id(), '<>');
      }
      $find_conditions = $query->orConditionGroup();
      $find_conditions
        ->condition('source', $path)
        ->condition('paths',"%<${path}>%", 'LIKE');
      $query->condition($find_conditions);
      $exports_with_path = $query->execute();
      if (count($exports_with_path) > 0) {
        continue;
      }

      $export_entity_target = $export->get('target')->entity;
      $export_destination_plugin = \Drupal::service('export.destination_manager')->createInstance($export_entity_target->get('destination'));
      $destination = $export_destination_plugin->getDestination();
      $namespace = $export_entity_target->get('namespace') ?: $export_entity_target->id();

      $export_entity_export_destination = $destination . $namespace . $path;

      // Allow export destination alterations.
      $event = new ExportEntityTransformDestinationEvent($export_entity_export_destination);
      \Drupal::service('event_dispatcher')->dispatch('export_entity.transform_destination', $event);
      $export_entity_export_destination = $event->getDestination();

      if (\Drupal::service('export_file.storage')->exists($export_entity_export_destination)) {
        continue;
      }

      $stream_wrappers = static::getRelevantStreamWrappers();

      $valid_stream_wrapper = false;
      foreach($stream_wrappers as $stream_wrapper => $stream_wrapper_data) {
        if (isset( $stream_wrapper_data['internal_path'])) {
          $possible_uri = $stream_wrapper . str_replace('/' . $stream_wrapper_data['internal_path'] . '/', '', $path);

          if (\Drupal::service('export_file.storage')->exists($possible_uri)) {
            \Drupal::service('export_file.storage')->copy($possible_uri, $destination . $namespace . $path);
            $valid_stream_wrapper = TRUE;
            break;
          }
        }
      }
      if ($valid_stream_wrapper) {
        continue;
      }

      try {
        // Fetch other path data.
        $response = \Drupal::service('http_client')->get(self::getHost() . $path);
        $content = $response->getBody()->getContents();
        \Drupal::service('export_file.storage')->set($content, $destination . $namespace . $path);
      } catch (\Exception $e) {
        // Do something useful here.
//       throw $e;
        $foo = 1;
      }
    }

    // Update & save export entity.
    $export
      ->set('paths', '<' . implode('> <', $paths) . '>')
      ->set('tags', '<' . implode('> <', $tags) . '>')
      ->set('expiry', $expiry)
      ->set('updated', (new \DateTime())->getTimestamp())
      ->set('health', TRUE)
      ->save();
  }

  /**
   * @param $export
   */
  protected static function remove(ExportEntityExport $export) {
    // Clean up previous export if export destination has changed.
    if ($export->id()) {
      /** @var \Drupal\export\Entity\Export $original_export */
      $original_export = \Drupal::service('entity_type.manager')
        ->getStorage('export_entity_export')
        ->loadUnchanged($export->id());
      if ($original_export->get('destination')->value !== $export->get('destination')->value) {
        \Drupal::service('export_file.storage')
          ->remove($original_export->get('destination')->value);
      }
    }

    // Remove export.
    \Drupal::service('export_file.storage')
      ->remove($export->get('destination')->value);

    // Remove export entity.
    $export->delete();
  }

  protected static function determine(EntityInterface $entity, ExportEntityTarget $export_entity_target) {
    /** @var \Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatInterface $export_entity_format_plugin */
    $export_entity_format_plugin = \Drupal::service('export_entity.format_manager')->createInstance($export_entity_target->get('format'));
    $format = $export_entity_format_plugin->getFormat();
    $source = $export_entity_format_plugin->getSource($entity);

    /** @var \Drupal\export\Plugin\ExportFormat\ExportFormatInterface $format_plugin */
    $export_destination_plugin = \Drupal::service('export.destination_manager')->createInstance($export_entity_target->get('destination'));
    $destination = $export_destination_plugin->getDestination();

    $namespace = $export_entity_target->get('namespace') ?: $export_entity_target->id();
    $alias = (bool) $export_entity_target->get('alias');

    $export_entity_export_storage = \Drupal::service('entity_type.manager')->getStorage('export_entity_export');

    $export_entity_export_data = [
      'entity' => $entity->getEntityTypeId() . ':' . $entity->id(),
      'target' => $export_entity_target->id(),
    ];

    $stored_export_entity_exports = $export_entity_export_storage->loadByProperties($export_entity_export_data);

    if (count($stored_export_entity_exports) > 0) {
      // Use existing export entity export.
      $export_entity_export = array_shift($stored_export_entity_exports);
    } else {
      // Add new export entity export.
      $export_entity_export = $export_entity_export_storage->create($export_entity_export_data);
    }

    $export_entity_export->set('format', $format);
    $export_entity_export->set('source', $source);
    $export_entity_export->set('namespace', $namespace);

    $export_entity_export_destination = $destination . $namespace . ($alias ? $entity->toUrl()->toString() : $source) . '.' . $format;

    // Allow export destination alterations.
    $event = new ExportEntityTransformDestinationEvent($export_entity_export_destination);
    \Drupal::service('event_dispatcher')->dispatch('export_entity.transform_destination', $event);
    $export_entity_export_destination = $event->getDestination();

    $export_entity_export->set('destination', $export_entity_export_destination);

   return [
      $export_entity_export,
    ];
  }

  protected static function determineTargets(EntityInterface $entity, $instant_update = FALSE) {
    $export_entity_targets = \Drupal::service('entity_type.manager')->getStorage('export_entity_target')->loadMultiple();

    return array_filter($export_entity_targets, function(ExportEntityTarget $export_entity_target) use ($entity, $instant_update) {
      if ($instant_update) {
        return in_array($entity->getEntityTypeId(), $export_entity_target->get('types'), TRUE) && $export_entity_target->get('status') && $export_entity_target->get('instant');
      } else {
        return in_array($entity->getEntityTypeId(), $export_entity_target->get('types'), TRUE) && $export_entity_target->get('status');
      }
    });
  }

  /**
   * @param $results
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  protected static function getTimeString($start) {
    try {
      $time = (new \DateTime())->getTimestamp() - $start;

      $hours = floor($time / 3600);
      $timestring = $hours ? new TranslatableMarkup('@hours hours, ', [
        '@hours' => $hours,
      ]) : '';

      $minutes = ($time / 60) % 60;
      $timestring .= $minutes ? new TranslatableMarkup('@minutes minutes and ', [
        '@minutes' => $minutes,
      ]) : '';

      $timestring .= new TranslatableMarkup('@seconds seconds', [
        '@seconds' => $seconds = $time % 60,
      ]);
      return $timestring;
    } catch (\Exception $e) {
      return 'an indefinite amount of time';
    }
  }

  protected static function deferMessages(&$context) {
    // Make sure we don't have any messages showing on exports, defer those.
    if (!isset($context['results'])) $context['results'] = [];
    if (!isset($context['results']['messages'])) $context['results']['messages'] = [];
    $context['results']['messages'] = \Drupal::messenger()->deleteAll() + $context['results']['messages'];
  }

  public static function determineParsedDependentPaths($export_entity_target, $instant_update, $amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['results']) || !isset($context['results']['dependents_queried']) || !isset($context['results']['dependent_ids'])) {
      $context['results']['dependents_queried'] = FALSE;
      $context['results']['dependent_ids'] = [];
    }
    if (empty($context['results']) || !isset($context['results']['dependents'])) {
      $context['results']['dependents'] = [];
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['results']['dependents_queried']) {
      // Build query to determine dependent exports.
      $query = \Drupal::entityQuery('export_entity_export');

      // Exclude handled exports.
      if (isset($context['results']['exclude']) && count($context['results']['exclude']) > 0) {
        $exclude_exports_conditions = $query->andConditionGroup();
        foreach ($context['results']['exclude'] as $exclude_export_id) {
          $exclude_exports_conditions->condition('id', $exclude_export_id, '<>');
        }
        $query->condition($exclude_exports_conditions);
      }

      /** @var EntityInterface $entity */
      $cache_tags_conditions = $query->orConditionGroup();
      $export_entity_target_conditions = $query->orConditionGroup();
      if ($export_entity_target && $export_entity_target->get('status')) {
        $export_entity_target_conditions->condition('target', $export_entity_target->id());
      }
      foreach ($context['results']['entities'] as $entity) {
        $cache_tags_to_invalidate = $entity->getCacheTagsToInvalidate();
        if (count($cache_tags_to_invalidate) > 0) {
          // Query for cache tag dependence.
          foreach ($cache_tags_to_invalidate as $cache_tag_to_invalidate) {
            $cache_tags_conditions->condition('tags',"%<${cache_tag_to_invalidate}>%", 'LIKE');
          }
        }
        if (!$export_entity_target) {
          $export_entity_targets = static::determineTargets($entity, $instant_update);
          if (!empty($export_entity_targets)) {
            foreach ($export_entity_targets as $export_entity_target) {
              if ($export_entity_target->get('status')) {
                $export_entity_target_conditions->condition('target', $export_entity_target->id());
              }
            }
          }
        }
      }
      if ($cache_tags_conditions->count()) {
        $query->condition($cache_tags_conditions);
      }
      if ($export_entity_target_conditions->count()) {
        $query->condition($export_entity_target_conditions);
      }

      try {
        $context['results']['dependent_ids'] = $query->execute();
      } catch (\Exception $e) {
        // TODO: do something useful here.
        throw $e;
      }

      $context['results']['dependents_queried'] = TRUE;
    }
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['dependent_ids']);
    }

    // Recompute dependent export entity exports as configuration might have
    // been updated since the last export.
    if (count($context['results']['dependent_ids']) > 0) {
      /** @var \Drupal\Core\Entity\EntityStorageInterface $export_storage */
      $export_entity_export_storage = \Drupal::service('entity_type.manager')->getStorage('export_entity_export');

      $dependent_ids = $context['results']['dependent_ids'];
      foreach ($export_entity_export_storage->loadMultiple(array_slice($dependent_ids, $context['sandbox']['progress'], $amount_per_execution, TRUE)) as $dependent_export) {
        $dependent_export_entity_target = $dependent_export->get('target')->entity;
        list($dependent_entity_type, $dependent_entity_id) = explode(':', $dependent_export->get('entity')->value);
        $dependent_entity = \Drupal::service('entity_type.manager')->getStorage($dependent_entity_type)->load($dependent_entity_id);
        $context['results']['dependents'] = array_merge(
          $context['results']['dependents'],
          static::determine($dependent_entity, $dependent_export_entity_target)
        );
      }
    }

    // Track the progress index.
    $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

    // Display progress message.
    $context['message'] = '<br />' . new TranslatableMarkup('Determined dependent exports for @progress of @total entities.', [
        '@progress' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['max'],
      ]);

    // Check if this operation is finished.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  public static function processParsedDependentPaths($amount_per_execution, &$context = []) {
    // Setup batch operation context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($context['results']['dependents']);
    }

    // Defer messages.
    static::deferMessages($context);

    if (!$context['sandbox']['max']) {
      $context['finished'] = 1;
    } else {
      // Process exports.
      $dependents = array_slice($context['results']['dependents'], $context['sandbox']['progress'], $amount_per_execution, TRUE);
      array_walk($dependents, function($export) {
        static::export($export);
      });

      // Track the progress index.
      $context['sandbox']['progress'] = $context['sandbox']['progress'] + $amount_per_execution;

      // Display progress message.
      $context['message'] = '<br />' . new TranslatableMarkup('Processed @progress of @total dependent exports.', [
          '@progress' => $context['sandbox']['progress'],
          '@total' => $context['sandbox']['max'],
        ]);

      // Check if this process is finished.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  protected static function getRelevantStreamWrappers() {
    $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
    $stream_wrappers = [];
    foreach (array_map(function ($stream_wrapper) use ($stream_wrapper_manager) {
      return $stream_wrapper_manager->getViaScheme($stream_wrapper);
    }, array_keys($stream_wrapper_manager->getWrappers())) as $stream_wrapper) {

      if (method_exists($stream_wrapper, 'basePath')) {
        $stream_wrappers[$stream_wrapper->getUri()]['internal_path'] = $stream_wrapper->basePath();
      }
      if ($stream_wrapper->getUri() !== 'private://' && method_exists($stream_wrapper, 'getExternalUrl')) {
        try {
          $stream_wrappers[$stream_wrapper->getUri()]['external_path'] = $stream_wrapper->getExternalUrl();
        } catch (\Exception $e) {
          $debug = 1;
//          $stream_wrappers[$stream_wrapper->getUri()]['internal_path'] = $stream_wrapper->basePath();
          $debug = 2;
        }
      }
    }
    return $stream_wrappers;
  }

  protected static function makeExternalUrlLocal($path) {
    $components = parse_url($path);
    if (UrlHelper::isExternal($path) && isset($components['host']) && UrlHelper::externalIsLocal($path, \Drupal::request()->getSchemeAndHttpHost())) {
      $path = $components['path'];
      if (!empty($components['query'])) {
        $path .= '?' . $components['query'];
      }
    }
    return $path;
  }

  /**
   * @return string
   */
  protected static function getHost() {
    $request = \Drupal::request();
    $host = $request->getHost();
    $scheme = $request->getScheme();
    $port = $request->getPort() ?: 80;
    if (('http' == $scheme && $port == 80) || ('https' == $scheme && $port == 443)) {
      $http_host = $scheme . '://' . $host;
    } else {
      $http_host = $scheme . '://' . $host . ':' . $port;
    }
    return $http_host;
  }

}
