<?php

namespace Drupal\export_entity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the export entity.
 *
 * @ingroup export
 *
 * @ContentEntityType(
 *   id = "export_entity_export",
 *   label = @Translation("Entity Export"),
 *   base_table = "export_entity",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/admin/export/entity/export/{export_entity_export}",
 *     "add-page" = "/admin/export/entity/export/add",
 *     "add-form" = "/admin/export/entity/export/add",
 *     "edit-form" = "/admin/export/entity/export/{export_entity_export}/edit",
 *     "delete-form" = "/admin/export/entity/export/{export_entity_export}/delete",
 *     "collection" = "/admin/export/entity/export",
 *   },
 *   admin_permission = "administer site configuration",
 * )
 */
class ExportEntityExport extends ContentEntityBase implements ContentEntityInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The exported entity'));

    $fields['target'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('The export target'))
      ->setSetting('target_type', 'export_entity_target')
      ->setSetting('handler', 'default');

    $fields['format'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The export format.'));

    $fields['source'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Path'))
      ->setDescription(t('The export data retrieval path.'));

    $fields['namespace'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The export namespace.'));

    $fields['destination'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The export destination path.'));

    $fields['paths'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('The export dependent paths like static assets.'));

    $fields['tags'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('The export cache tags.'));

    $fields['expiry'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('The export cache expiry as a UNIX timestamp.'));

    $fields['updated'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('The export time as a unix timestamp.'));

    $fields['health'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('The health of the previous export operation.'));

    return $fields;
  }

}
