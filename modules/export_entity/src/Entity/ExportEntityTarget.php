<?php

namespace Drupal\export_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the target entity.
 *
 * @ConfigEntityType(
 *   id = "export_entity_target",
 *   label = @Translation("Entity Target"),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\export_entity\Controller\ExportEntityTargetListBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\export_entity\Form\ExportEntityTargetForm",
 *       "add" = "Drupal\export_entity\Form\ExportEntityTargetForm",
 *       "edit" = "Drupal\export_entity\Form\ExportEntityTargetForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   links = {
 *     "canonical" = "/admin/export/entity/target/{export_entity_target}",
 *     "add-form" = "/admin/export/entity/target/add",
 *     "edit-form" = "/admin/export/entity/target/{export_entity_target}/edit",
 *     "delete-form" = "/admin/export/entity/target/{export_entity_target}/delete",
 *     "collection" = "/admin/export/entity/target",
 *   },
 *   admin_permission = "administer site configuration",
 *   config_prefix = "target",
 *   config_export = {
 *     "id",
 *     "label",
 *     "types",
 *     "format",
 *     "destination",
 *     "namespace",
 *     "alias",
 *     "instant",
 *     "status",
 *   },
 * )
 */
class ExportEntityTarget extends ConfigEntityBase implements ConfigEntityInterface {

  /**
   * The target ID.
   *
   * @var string
   */
  public $id;

  /**
   * The target label.
   *
   * @var string
   */
  public $label;

  public $types;

  public $format;

  public $destination;

  public $namespace;

  public $alias;

  public $instant;

  public $status;

}
