<?php

namespace Drupal\export_entity\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Provides an event for export data parsing & processing.
 */
class ExportEntityDetermineDependentPathsEvent extends Event {

  /**
   * Export data.
   *
   * @var string
   */
  protected $data;

  protected $export;

  protected $paths;

  /**
   * Constructs an event.
   *
   * @param string $data
   *   Export data.
   */
  public function __construct($data, $export) {
    $this->data = $data;
    $this->export = $export;
    $this->paths = [];
  }

  /**
   * Gets the export data.
   *
   * @return string
   *   The export data.
   */
  public function getData() {
    return $this->data;
  }

  public function getExport() {
    return $this->export;
  }

  public function setPaths($paths) {
    $this->paths = $paths;
  }

  public function getPaths() {


    $debug = 1;
    return $this->paths;
  }

}
