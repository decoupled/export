<?php

namespace Drupal\export_entity\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Provides an event for export data parsing & processing.
 */
class ExportEntityTransformDestinationEvent extends Event {

  /**
   * Export destination.
   *
   * @var string
   */
  protected $destination;

  /**
   * Constructs an event.
   *
   * @param string $data
   *   Export data.
   */
  public function __construct($destination) {
    $this->destination = $destination;
  }

  /**
   * Gets the export destination.
   *
   * @return string
   *   The export data.
   */
  public function getDestination() {
    return $this->destination;
  }

  public function setDestination($destination) {
    $this->destination = $destination;
  }

}
