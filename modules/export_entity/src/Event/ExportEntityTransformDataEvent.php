<?php

namespace Drupal\export_entity\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Provides an event for export data parsing & processing.
 */
class ExportEntityTransformDataEvent extends Event {

  /**
   * Export data.
   *
   * @var string
   */
  protected $data;

  protected $export;

  protected $paths;

  /**
   * Constructs an event.
   *
   * @param string $data
   *   Export data.
   */
  public function __construct($data, $export) {
    $this->data = $data;
    $this->export = $export;
  }

  /**
   * Gets the export data.
   *
   * @return string
   *   The export data.
   */
  public function getData() {
    return $this->data;
  }

  public function getExport() {
    return $this->export;
  }

  public function setData($data) {
    $this->data = $data;
  }

}
