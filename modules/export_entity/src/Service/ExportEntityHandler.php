<?php

namespace Drupal\export_entity\Service;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\export_entity\Batch\ExportEntityBatchUtility;
use Drupal\export_entity\Entity\ExportEntityTarget;

/**
 * Class ExportEntityHandler
 *
 * @package Drupal\export_entity\Service
 */
class ExportEntityHandler {

  protected $excludes = [];

  public function upsertMultipleByTypeAndId($entity_type, array $entity_ids, ExportEntityTarget $export_entity_target = NULL, $with_dependencies = TRUE, $instant_update = FALSE) {
    $batch = (new BatchBuilder())
      ->setInitMessage(new TranslatableMarkup('Preparing exports'))
      ->setTitle(new TranslatableMarkup('Processing exports'))
      ->setProgressMessage(new TranslatableMarkup('Bulk export running for @elapsed.'))
      ->setErrorMessage(new TranslatableMarkup('There was a problem processing the exports.'))
      ->addOperation([ExportEntityBatchUtility::class, 'setup'])
      ->addOperation([ExportEntityBatchUtility::class, 'loadEntities'], [$entity_type, $entity_ids, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'determineExports'], [$export_entity_target, $instant_update, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'processExports'], [10]);

    if ($with_dependencies) {
      $batch
        ->addOperation([ExportEntityBatchUtility::class, 'determineDependentExports'], [$export_entity_target, $instant_update, 100])
        ->addOperation([ExportEntityBatchUtility::class, 'processDependentExports'], [10]);
    }

    $batch
      ->addOperation([ExportEntityBatchUtility::class, 'determineParsedDependentPaths'], [$export_entity_target, $instant_update, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'processParsedDependentPaths'], [10]);

    $batch
      ->setFinishCallback([ExportEntityBatchUtility::class, 'finish']);

    batch_set($batch->toArray());
  }

  public function upsert(EntityInterface $entity, ExportEntityTarget $export_entity_target = NULL, $with_dependencies = TRUE, $instant_update = FALSE) {
    $this->upsertMultiple([$entity], $export_entity_target, $with_dependencies, $instant_update);
  }

  public function upsertMultiple(array $entities, ExportEntityTarget $export_entity_target = NULL, $with_dependencies = TRUE, $instant_update = FALSE) {
    $batch = (new BatchBuilder())
      ->setInitMessage(new TranslatableMarkup('Preparing exports'))
      ->setTitle(new TranslatableMarkup('Processing exports'))
      ->setProgressMessage(new TranslatableMarkup('Bulk export running for @elapsed.'))
      ->setErrorMessage(new TranslatableMarkup('There was a problem processing the exports.'))
      ->addOperation([ExportEntityBatchUtility::class, 'setup'])
      ->addOperation([ExportEntityBatchUtility::class, 'addEntities'], [$entities])
      ->addOperation([ExportEntityBatchUtility::class, 'determineExports'], [$export_entity_target, $instant_update, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'processExports'], [10]);

    if ($with_dependencies) {
      $batch
        ->addOperation([ExportEntityBatchUtility::class, 'determineDependentExports'], [$export_entity_target, $instant_update, 100])
        ->addOperation([ExportEntityBatchUtility::class, 'processDependentExports'], [10]);
    }

    $batch
      ->addOperation([ExportEntityBatchUtility::class, 'determineParsedDependentPaths'], [$export_entity_target, $instant_update, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'processParsedDependentPaths'], [10]);

    $batch
      ->setFinishCallback([ExportEntityBatchUtility::class, 'finish']);

    batch_set($batch->toArray());
  }

  public function delete(EntityInterface $entity, ExportEntityTarget $export_entity_target = NULL, $with_dependencies = TRUE, $instant_update = FALSE) {
    $this->deleteMultiple([$entity], $export_entity_target, $with_dependencies, $instant_update);
  }

  public function deleteMultiple(array $entities, ExportEntityTarget $export_entity_target = NULL, $with_dependencies = TRUE, $instant_update = FALSE) {
    $batch = (new BatchBuilder())
      ->setInitMessage(new TranslatableMarkup('Preparing export removal'))
      ->setTitle(new TranslatableMarkup('Processing export removal'))
      ->setProgressMessage(new TranslatableMarkup('Bulk export removal running for @elapsed.'))
      ->setErrorMessage(new TranslatableMarkup('There was a problem removing the exports.'))
      ->addOperation([ExportEntityBatchUtility::class, 'setup'])
      ->addOperation([ExportEntityBatchUtility::class, 'addEntities'], [$entities])
      ->addOperation([ExportEntityBatchUtility::class, 'determineExports'], [$export_entity_target, $instant_update, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'removeExports'], [100]);

    if ($with_dependencies) {
      $batch
        ->addOperation([ExportEntityBatchUtility::class, 'determineDependentExports'], [$export_entity_target, $instant_update, 100])
        ->addOperation([ExportEntityBatchUtility::class, 'processDependentExports'], [10]);
    }

    // @TODO: Figure out how to remove obsolete static assets.

    $batch
      ->setFinishCallback([ExportEntityBatchUtility::class, 'finish']);

    batch_set($batch->toArray());
  }

  public function clear(ExportEntityTarget $export_entity_target) {

    $export_entity_ids = \Drupal::entityQuery('export_entity_export')
      ->condition('target', $export_entity_target->id())
      ->execute();

    $batch = (new BatchBuilder())
      ->setInitMessage(new TranslatableMarkup('Preparing export removal'))
      ->setTitle(new TranslatableMarkup('Processing export removal'))
      ->setProgressMessage(new TranslatableMarkup('Bulk export removal running for @elapsed.'))
      ->setErrorMessage(new TranslatableMarkup('There was a problem removing the exports.'))
      ->addOperation([ExportEntityBatchUtility::class, 'setup'])
      ->addOperation([ExportEntityBatchUtility::class, 'loadExports'], [$export_entity_ids, 100])
      ->addOperation([ExportEntityBatchUtility::class, 'removeExports'], [100])
      ->setFinishCallback([ExportEntityBatchUtility::class, 'finish']);

    batch_set($batch->toArray());
  }

}
