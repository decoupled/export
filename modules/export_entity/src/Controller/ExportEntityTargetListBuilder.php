<?php

namespace Drupal\export_entity\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of target configuration entities.
 *
 * @ingroup export
 */
class ExportEntityTargetListBuilder extends ConfigEntityListBuilder {

  /**
   * @inheritdoc
   *
   * @return array
   *   A list of field labels.
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['types'] = $this->t('Types');
    $header['format'] = $this->t('Format');
    $header['destination'] = $this->t('Destination');
    $header['namespace'] = $this->t('Namespace');
    return $header + parent::buildHeader();
  }

  /**
   * @inheritdoc
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The target configuration entity.
   *
   * @return array
   *   A list of field values.
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];

    $row['label'] = $entity->toLink(NULL, 'edit-form');
    $row['status'] = ((bool) $entity->get('status')) === TRUE ? '✔' : '✖';

    $types = $entity->get('types');

    $all_types = array_map(function($entity_type) {
      return $entity_type->getLabel();
    }, \Drupal::entityTypeManager()->getDefinitions());

    $configured_types = array_filter($all_types, function($type) use ($types) {
      return in_array($type, array_values($types), TRUE);
    }, ARRAY_FILTER_USE_KEY);
    $row['types'] = implode(', ', $configured_types);

    $row['format'] = $entity->get('format');
    $row['destination'] = $entity->get('destination');
    $row['namespace'] = $entity->get('namespace') ? $entity->get('namespace') : $entity->id();

    return $row + parent::buildRow($entity);
  }

}
