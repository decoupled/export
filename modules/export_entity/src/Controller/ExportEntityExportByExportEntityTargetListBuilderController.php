<?php

namespace Drupal\export_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\export_entity\Entity\ExportEntityTarget;


/**
 * Defines a generic controller to list entities.
 */
class ExportEntityExportByExportEntityTargetListBuilderController extends ControllerBase {

  /**
   * Provides the listing page for any entity type.
   *
   * @param string $entity_type
   *   The entity type to render.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function listing(ExportEntityTarget $export_entity_target, $type) {
    $entity_type = $this->entityTypeManager()->getDefinition($type);
    $list_builder = ExportEntityExportByExportEntityTargetListBuilder::createInstance(\Drupal::getContainer(), $entity_type);
    return $list_builder->renderList($export_entity_target);
  }

  /**
   * The _title_callback for the node.add route.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $export_entity_target, $type) {
    return $this->t('Exports for @name', ['@name' => $export_entity_target->label()]);
  }

}
