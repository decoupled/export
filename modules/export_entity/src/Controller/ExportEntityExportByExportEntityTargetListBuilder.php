<?php

namespace Drupal\export_entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\export_entity\Entity\ExportEntityTarget;


class ExportEntityExportByExportEntityTargetListBuilder extends EntityListBuilder {

  /**
   * @inheritdoc
   */
//  protected $limit = FALSE;
  protected $limit = 100;

  /**
   * @var \Drupal\export_entity\Entity\ExportEntityTarget
   */
  protected $export_entity_target;

  public function buildHeader() {
    $header = [];
    $header['entity'] = $this->t('Target entity');
    $header['health'] = $this->t('Health');
    $header['source'] = $this->t('Source');
    $header['destination'] = $this->t('Destination');
    $header['paths'] = $this->t('Included paths');
    $header['tags'] = $this->t('Cache tags');
    $header['expiry'] = $this->t('Expiry');
    $header['updated'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {

    $row = [];

    list ($target_entity_type, $target_entity_id) = explode(':', $entity->get('entity')->value);
    $target_entity = \Drupal::service('entity_type.manager')->getStorage($target_entity_type)->load($target_entity_id);

    if ($target_entity) {
      /** @var \Drupal\Core\Entity\EntityInterface $target_entity */
      $row['entity'] = [
        'data' => $target_entity->hasLinkTemplate('canonical') ? $target_entity->toLink(NULL, 'canonical') : $target_entity->label(),
        'style' => 'width: 15%; word-break:break-all;',
      ];
      $row['health'] = ((bool) $entity->get('health')->value === TRUE) ? '✔' : '✖';
      $row['source'] = Link::fromTextAndUrl($this->t('Visit'), Url::fromUri('internal:' . $entity->get('source')->value))->toString();
      $row['destination'] = Link::fromTextAndUrl($this->t('Visit'), Url::fromUri(file_create_url($entity->get('destination')->value)))->toString();

      $paths = $entity->get('paths')->value;
      $paths = str_replace('<', '', $paths);
      $paths = str_replace('>', '', $paths);
      $row['paths'] = [
        'data' => $paths,
        'style' => 'font-size: 75%;'
      ];

      $tags = $entity->get('tags')->value;
      $tags = str_replace('<', '', $tags);
      $tags = str_replace('>', '', $tags);
      $row['tags'] = [
        'data' => $tags,
        'style' => 'font-size: 75%;'
      ];

      $row['expiry'] = ($entity->get('expiry')->value === '-1') ? '✖' : $entity->get('expiry')->value;
      $row['updated'] = [
        'data' => \Drupal::service('date.formatter')->format($entity->get('updated')->value),
        'style' => 'width: 15%;',
      ];
    }

    return $row + parent::buildRow($entity);
  }

  public function renderList(ExportEntityTarget $export_entity_target) {
    $this->export_entity_target = $export_entity_target;
    return parent::render();
  }

  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('target', $this->export_entity_target->id())
      ->sort('health', 'ASC')
      ->sort('updated', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
