<?php

namespace Drupal\export_entity\Plugin\ExportEntityFormat;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a common interface for all ExportFormat objects.
 */
interface ExportEntityFormatInterface {

    public function getFormat();

    public function getSource(EntityInterface $entity);

}
