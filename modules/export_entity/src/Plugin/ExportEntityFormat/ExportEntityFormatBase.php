<?php

namespace Drupal\export_entity\Plugin\ExportEntityFormat;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\export_entity\Plugin\ExportEntityFormat\ExportEntityFormatInterface;

/**
 * Defines a common interface for all ExportFormat objects.
 */
abstract class ExportEntityFormatBase implements ExportEntityFormatInterface {

    public function getFormat() {}

    public function getSource(EntityInterface $entity) {}

}
