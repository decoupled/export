<?php

namespace Drupal\export_request;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\export_request\EventSubscriber\PageCacheRequestPrepareSubscriber;
use Drupal\export_request\PageCache\RequestPolicy\DynamicRequestPolicy;
use Drupal\export_request\Service\ExportRequestProcessExportHandler;
use Drupal\export_request\StackMiddleware\ResettablePageCache;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers services in the container.
 */
class ExportRequestServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');

    $container->register('export_request.handler', ExportRequestProcessExportHandler::class);

    $context_repository_definition = new ChildDefinition('context.repository');
    $context_repository_definition->setClass(LazyResettableContextRepository::class);
    $context_repository_definition->setDecoratedService('context.repository');
    $container->setDefinition('export_request.context.repository', $context_repository_definition);

    $path_matcher_definition = new ChildDefinition('path.matcher');
    $path_matcher_definition->setClass(ResettablePathMatcher::class);
    $path_matcher_definition->setDecoratedService('path.matcher');
    $container->setDefinition('export_request.path.matcher', $path_matcher_definition);

    $container->register('export_request.request_preparer', RequestPreparer::class)
      ->addArgument(new Reference('entity_type.manager'))
      ->addArgument(new Reference('context.repository'))
      ->addArgument(new Reference('path.matcher'))
      ->addArgument(new Reference('menu.active_trail'))
      ->addArgument(new Reference('event_dispatcher'));

    if (isset($modules['dynamic_page_cache'])) {
      $container->register('export_request.dynamic_page_cache_request_policy', DynamicRequestPolicy::class)
        ->setDecoratedService('dynamic_page_cache_request_policy');
    }

    if (isset($modules['page_cache'])) {
      $container->setDefinition('export_request.http_middleware.page_cache', new ChildDefinition('http_middleware.page_cache'))
        ->setClass(ResettablePageCache::class)
        ->setDecoratedService('http_middleware.page_cache');
      $container->register('export_request.page_cache_request_prepare_subscriber', PageCacheRequestPrepareSubscriber::class)
        ->addTag('event_subscriber')
        ->addArgument(new Reference('http_middleware.page_cache'));
    }

  }

}
