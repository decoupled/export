<?php

namespace Drupal\export_request\EventSubscriber;

use Drupal\export_request\StackMiddleware\ResettablePageCache;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Prepares for a new request when using page cache.
 *
 * @internal
 */
class PageCacheRequestPrepareSubscriber implements EventSubscriberInterface {

  /**
   * The resettable page cache.
   *
   * @var \Drupal\export_request\StackMiddleware\ResettablePageCache
   */
  protected $pageCache;

  /**
   * Constructs the EntityPathSubscriber object.
   *
   * @param \Drupal\export_request\StackMiddleware\ResettablePageCache $page_cache
   *   The resettable page cache.
   */
  public function __construct(ResettablePageCache $page_cache) {
    $this->pageCache = $page_cache;
  }

  /**
   * Reacts to a collect paths event.
   */
  public function prepareForRequest() {
    $this->pageCache->resetCache();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['export_request.request_prepare'][] = ['prepareForRequest'];
    return $events;
  }

}
