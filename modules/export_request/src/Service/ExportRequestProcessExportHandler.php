<?php

namespace Drupal\export_request\Service;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class ExportRequestProcessExportHandler {

  public function handleExport($export) {
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo(new AnonymousUserSession());

    $source = $export->get('source')->value;

    // This is where the export resource is actually fetched. This does seem to
    // cause some problems, also see https://cgit.drupalcode.org/tome/tree/modules/tome_static/src/RequestPreparer.php
    // Some contrib modules, like metatag, statically cache route data.

    // START

    \Drupal::service('export_request.request_preparer')->prepareForRequest();

    // END

    $request = Request::create($source, 'GET', [], [], [], \Drupal::request()->server->all());
    try {
      /** @var \Symfony\Component\HttpFoundation\Response $response */
      $response = \Drupal::service('http_kernel')->handle($request, HttpKernelInterface::MASTER_REQUEST);
    } catch (\Exception $e) {
      // Do something useful here.
      $account_switcher->switchBack();
      throw $e;
    }

    $account_switcher->switchBack();

    $cache_tags = $response instanceof CacheableResponseInterface ? $response->getCacheableMetadata()->getCacheTags() : [];
    $expiry = $response instanceof CacheableResponseInterface && $response->getCacheableMetadata()->getCacheMaxAge() !== -1 ? $response->getCacheableMetadata()->getCacheMaxAge() + (new \DateTime())->getTimestamp() : -1;

    return [
      'data' => $response->getContent(),
      'tags' => $cache_tags,
      'expiry' => $expiry,
    ];
  }

}
