INTRODUCTION
------------

You should probably not use this. Might break everything.

The Export module suite provides content and/or config exports utilizing an extendable export pipeline supporting 
various source formats, transformations and destinations via Drupal's plugin APIs.

The primary current purpose of this module is a more or less static exported state of Drupal's core entity types via 
JSON:API in a filesystem as part of a proof-of-concept JAMstack architecture w/ Drupal as editorial interface.

Significant parts of this module, both in spirit and in source code, are inspired by & directly taken from the awesome 
[Tome](https://www.drupal.org/project/tome) project, a fully functioning OOTB solution for exporting a Drupal site into 
a working static copy.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/export

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/export
   
INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------
 
 * Configure an export target in Administration » Configuration » Web services » Entity export targets.

MISCELLANEOUS
-------------

 * There is some support for [Flysystem](https://www.drupal.org/project/flysystem) module baked in, but local adapters 
 currently only work with the `public` option set to `TRUE`.
